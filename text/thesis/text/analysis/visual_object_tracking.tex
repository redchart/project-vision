Visual object tracking (VOT) is an essential part of computer vision. With powerful computers, good quality and affordable video cameras, and the need for automatic video analysis, people are more interested in object tracking algorithms. There are three main steps in analyzing videos: finding interesting moving objects, following these objects from one frame to another, and studying their movement to understand their behavior. Therefore, object tracking is beneficial for tasks like:
\begin{enumerate}
    \item Traffic monitoring
    \item Industrial robotics
    \item Vehicle tracking
    \item Vehicle navigation~\cite{VOTsurvay,VOTAhmad}
\end{enumerate}

In its most basic form, tracking refers to the process of determining an object's path in the image plane as it moves throughout a scene. In essence, a tracker assigns consistent labels to objects being tracked across various video frames. Furthermore, depending on the tracking domain, a tracker can also provide object-specific information, such as orientation, area, or shape~\cite{VOTsurvay}.
Researchers have put a lot of work into visual object tracking (VOT) for the past 40 years. However, it remains an open area for computer vision research because of various challenges. These issues can be described as follows:
\begin{enumerate}
    \item Occlusion -- This problem occurs when a target is partially or fully hidden by another object, presenting a common challenge during tracking. There is no universal technique to address it, so strategies are chosen on the basis of the target's nature and the tracking environment.
    \item Changing appearance -- Targets can change appearance during motion, so the model must adapt for long-term tracking. The stability vs. plasticity dilemma is to find the right balance between stability and adaptability.
    \item Changing target size in image -- As a target moves closer or farther from the camera, its size in the image changes, so tracking strategies need to account for this.
    \item Noise in the image -- If the image from the target scene has noise, some preprocessing is needed to remove it.~\cite{VOTsurvay,VOTAhmad}

\end{enumerate}


Object tracking methods can be classified in various ways. The authors of "Object Tracking Methods:A Review" described different categories of object detection techniques and used classification, which is shown in figure ~\ref{fig:tracking_classification} 

\begin{figure}[h]
    \caption{Classification of object tracking methods~\cite{8964761figures}}
    \centering
    \includegraphics[width=1\textwidth]{text/images/tracking_classification.png}
    \label{fig:tracking_classification}
\end{figure}

Feature-based tracking is a simple method where unique features like color, texture, and optical flow are extracted to distinguish objects. After extraction, the most similar object in the next frame is identified using a similarity criterion. The challenge lies in extracting unique and reliable features to differentiate the target from other objects. The feature-based methods use the following features:
\begin{enumerate}
    \item Color -- Color features represent an object's appearance and can be used in various ways, such as color histograms, which display the distribution of colors and the pixel count for each color in an image. However, color histograms only consider color, not shape or texture, so two different objects may have the same histogram.
    \item Optical flow -- Optical flow refers to the perceived motion of brightness patterns in an image, which may result from lighting changes without actual movement. Optical flow algorithms measure the displacement of these patterns between frames. Dense optical streaming algorithms calculate displacement for all pixels, while light-flow algorithms estimate displacement for a select number of pixels in an image.
    \item  Texture -- Texture features, derived from image preprocessing techniques, represent repeated patterns or structures. They can be combined with color features to better describe an image or region. Gabor wavelets, which are invariant to illumination, rotation, scale, and translation, are a popular texture feature.~\cite{VOTsingle, trackingOverview}
\end{enumerate}

Segmenting foreground objects from a video frame is essential and critical for visual tracking. The foreground segmentation separates moving objects from the background scene. To track these objects, they must be distinguished from the background. There are the following object tracking methods based on segmentation:
\begin{enumerate}
    \item Bottom-Up based method -- In this tracking approach, two distinct tasks are performed: foreground segmentation followed by object tracking. Foreground segmentation involves low-level segmentation to extract regions in all frames. Then, features are extracted from foreground regions and used for tracking based on those features.
    \item Joint Based Method -- In the bottom-up method, foreground segmentation and tracking are separate tasks, leading to segmentation errors that propagate and cause tracking errors. To address this problem, researchers combined the foreground segmentation and tracking method, which improved the tracking performance.~\cite{VOTsingle, trackingOverview}
\end{enumerate}

Estimation methods turn tracking into an estimation problem by representing an object with a state vector that describes its dynamics, such as position and velocity. Using Bayesian filters, these methods continuously update the target's position based on sensor data through a two-step process: prediction and updating. The prediction estimates the target's next position, while updating refines that position with current observations, and this cycle is repeated for each video frame. There are following object tracking methods based on estimation:
\begin{enumerate}
    \item Kalman filter -- The Kalman filter is utilized in object tracking by designing a dynamic model for target movement. It estimates the position of a linear system with Gaussian errors. If the dynamic models are nonlinear, the Kalman filter is not suitable, and alternatives such as the extended Kalman filter are employed instead.
    \item Particle filter -- Particle filters help track objects in complex situations using particles and probabilities. They can handle challenges like non-Gaussian noise but sometimes need resampling to fix issues with high probability particles.~\cite{VOTsingle, trackingOverview}
\end{enumerate}

Learning-based methods for tracking learn features and appearances of targets to predict their positions in future frames. These methods can be categorized into generative, discriminative and reinforcement learning approaches.~\cite{VOTsingle, trackingOverview}

 Generative methods concentrate on searching areas similar to the object, with Correlation Filter-based trackers being a prime example. The main concept involves estimating an optimal image filter to generate an ideal output in the input image. The target is identified in the first frame, and the filter is trained on it. Then, at each step, the patch is cropped to its predicted position for tracking, features are extracted, and a spatial confidence map is obtained. Finally, the new target position is predicted and the correlation filter is updated accordingly.~\cite{VOTsingle, trackingOverview}
 
Discriminative trackers often treat tracking as a classification issue, differentiating the target from the background (Siamese tracking, Patch learning tracker,Graph-based tracker).~\cite{VOTsingle, trackingOverview}

Reinforcement learning involves an agent that interacts with the environment through trial and error to select the optimal action to achieve a goal. Some studies use reinforcement learning for tracking, such as an approach divided into a matching network and a policy network. Given a frame, a search image is cropped based on previous target information. Using appearance templates from previously tracked frames, the matching network generates prediction maps. The policy network then scores each map, selecting the one with the highest score for target tracking. The policy network is trained to handle various situations.~\cite{VOTsingle, trackingOverview}
