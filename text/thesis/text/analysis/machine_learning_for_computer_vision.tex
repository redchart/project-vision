Traditional object detection methods are based on hand-crafted features and shallow learning architectures, which in turn impose limitations on their capabilities. Deep learning methods significantly outperform them by being able to learn semantics and detect deeper features.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

A convolutional neural network (CNN) is a kind of artificial neural network that employs numerous perceptrons to examine image inputs. It utilizes learnable weights and biases for different parts of images, enabling them to be distinguished from one another. One benefit of using a CNN is that it takes advantage of local spatial coherence in input images, allowing for shared parameters, and thus fewer weights. This approach is notably efficient in terms of memory and complexity. The fundamental components of a convolutional neural network include the following:
\begin{enumerate}
    \item Convolution layer -- In the convolutional layer, a kernel matrix is moved across the input matrix to generate a feature map for the subsequent layer. This is done by performing a mathematical operation called convolution, which involves sliding the kernel matrix over the input matrix and executing element-wise matrix multiplication at each position, with the results summed onto the feature map.
    \item Nonlinear activation functions (ReLU) -- The activation function is a nonlinear transformation applied to the input signal, following the convolutional layer. For example, the rectified linear unit (ReLU) activation function outputs the input if it is positive and zero if it is negative.
    \item Pooling Layer -- A problem with feature maps from convolutional layers is that they track exact feature positions, so small changes in the input image can create very different maps. To solve this problem, a pooling layer is used after the nonlinearity layer, which helps to keep the output mostly the same even if the input shifts slightly.
    \item Fully Connected Layer -- In a convolutional neural network, the final pooling layer's output serves as input for the fully connected layer(s). Fully connected implies that each node in one layer is connected to every node in the subsequent layer.~\cite{tammina2019transfer}
\end{enumerate}

% The Convolutional Neural Network (CNN) is the most representative deep learning model.

The object detection task consists of localizing the object and then classifying it. Therefore, two groups of detectors are distinguished: "two-stage detectors" and "one-stage detectors." The former separately addresses the localization and classification tasks, while the latter solves these tasks simultaneously. The first group includes the following:
Traditional object recognition methods include:
\begin{enumerate}
    \item RCNN
    \item SPPNet
    \item Fast RCNN
    \item Faster RCNN
    \item Feature Pyramid Networks (FPNs)~\cite{DeepObjectDetection, ObjectDetectionIn20Years}
\end{enumerate}

The RCNN is based on a simple idea that is shown in ~\ref{fig:rcnn} initially employs a selective search to identify regions of the image, which are then passed to CNN for feature extraction. These features are then sent to a linear SVM for object classification within the region. However, despite the significant improvements in object detection achieved by RCNN, there is a substantial drawback in redundant feature computations in overlapping regions, resulting in inefficient calculations.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

\begin{figure}[h]
    \caption{Architecture of R-CNN~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.6\textwidth]{text/images/rcnn.png}
    \label{fig:rcnn}
\end{figure}

SPPNet resolved the issue of the previous method, which limited the input image sizes and required feature extraction to be performed in each detection iteration. SPPNet proposed a solution to these problems by extracting features only once for the entire image and adding a Spatial Pyramid Pooling (SPP) layer as shown in ~\ref{fig:sspnet}, which allows feature maps to be generated independently of the image size. With almost 20 times the acceleration of RCNN without sacrificing detection accuracy, SPPNet significantly improved computational efficiency. However, the problems of multistep training and the fact that SPPNet only fine-tunes its fully connected layers, ignoring all previous layers, remained.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

\begin{figure}[h]
    \caption{SPPnet architecture~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.6\textwidth]{text/images/sspnet.png}
    \label{fig:sspnet}
\end{figure}

Fast RCNN was introduced in 2015 by Girshick et al. and presented an improvement over RCNN and SPPNet. Schematically depicted in ~\ref{fig:fast_rcnn}, the working principle involves performing feature extraction on the entire image, unlike its predecessors. Subsequently, a fixed-length feature vector is created from these features using Region of Interest (ROI) pooling, which is a specific form of Spatial Pyramid Pooling. Each vector is then passed to the Fully Connected (FC) layers, sending their output to a classifier and bounding box regressor. Despite addressing the issues of its predecessors, computational speed problems persisted due to the inefficiency of proposal detection.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

\begin{figure}[h]
    \caption{Architecture of Fast R-CNN~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.8\textwidth]{text/images/fastRcnn.png}
    \label{fig:fast_rcnn}
\end{figure}

Faster RCNN was introduced shortly after Fast RCNN by Ren et al. This method was the first capable of detecting objects almost in real time. This inference speed was made possible through a Region Proposal Network (RPN), which efficiently locates object-containing regions of the image (regardless of object class) and then forwards them along with the feature map to the box-classification and box-regression layers. Limitations of this approach include the resource-intensive nature of the RPN, which can only identify object-shaped regions and struggle with large objects of size and shape.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

Feature Pyramid Networks (FPNs) were introduced by Lin et al. in 2017. Before this, the Feature Pyramid principle had been utilized to enhance the detection of images of varying sizes, but its implementation by the FPN predecessors could have been more efficient. It is also worth noting that most methods construct detection from the bottom up, which means that increasingly complex features are composed of more minor features that influence detection. FPN was the first to adopt an alternative principle, as shown in ~\ref{fig:fpn}, whereby features at lower levels could significantly affect object detection. This novel approach to feature processing constitutes the primary contribution of FPN. FPN has become a fundamental component of many contemporary architectures.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

\begin{figure}[h]
    \caption{Main concern of FPN~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.6\textwidth]{text/images/fpn.png}
    \label{fig:fpn}
\end{figure}

Considering their complexity and slow speed, two-stage detectors are rarely used to solve contemporary problems. Although one-stage detectors may have lower accuracy, they are frequently used due to their speed and ability to function on mobile devices~\cite{DeepObjectDetection, ObjectDetectionIn20Years}.

The "One-stage detectors" group includes:
\begin{enumerate}
    \item You Only Look Once (YOLO)
    \item Single-Shot Multibox Detector (SSD)
    \item RetinaNet~\cite{DeepObjectDetection, ObjectDetectionIn20Years}
\end{enumerate}

You Only Look Once was introduced by Joseph et al. in 2015. The main idea, illustrated in ~\ref{fig:yolo}, involves dividing the image into a grid of S x S where each cell is responsible for predicting whether an object is present and the confidence of the algorithm in this prediction. As a result, bounding boxes are created. Then, the likelihood of each class being present in a cell is calculated independently of the number of bounding boxes, and only cells containing a class influencing the creation of the bounding box.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}

\begin{figure}[h]
    \caption{Main concern of YOLO~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.6\textwidth]{text/images/yolo.png}
    \label{fig:yolo}
\end{figure}

Single-Shot Multibox Detector was introduced by Liu et al. in 2015. The main contribution of this method was the significantly improved accuracy of the one-stage detectors. SSD aims to improve the detection of small objects in groups, an issue with which YOLO needed help. The SSD architecture, depicted in ~\ref{fig:ssd}, consists of a base CNN with additional convolutional layers added at the end, which helps to identify candidates for the bounding box with greater precision. Eight thousand seven hundred thirty-two bounding box candidates are obtained from the convolutional layers' output. Subsequently, Nonmaximum Suppression filters only 200 bounding boxes for each class.~\cite{DeepObjectDetection, ObjectDetectionIn20Years}
\begin{figure}[h]
    \caption{The SSD architecture~\cite{8627998figures} }
    \centering
    \includegraphics[width=0.9\textwidth]{text/images/ssd.png}
    \label{fig:ssd}
\end{figure}

RetinaNet was introduced by Lin et al. in 2017. All previous one-stage detector methods shared a common drawback: the calculation process generated excessive (often useless) bounding box candidates, negatively impacting training results. RetinaNet not only achieved but also surpassed the accuracy of two-stage detectors by addressing this issue. This detection accuracy was achieved by applying a new error calculation method during training, which helps focus on negative results. The RetinaNet schema is shown in ~\ref{fig:retinanet}.~\cite{retinanet, DeepObjectDetection, ObjectDetectionIn20Years}
\begin{figure}[h]
    \caption{The RetinaNet schema~\cite{8627998figures} }
    \centering
    \includegraphics[width=1\textwidth]{text/images/retinanet.png}
    \label{fig:retinanet}
\end{figure}






