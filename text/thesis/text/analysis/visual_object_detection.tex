%  Object detection is a fundamental and complex problem that allows capturing objects from real-world scenes using camera data, such as human faces, automobiles, animals, and others. The object detection task is divided into two subtasks. The first is the location of a given object in the image. The second is the classification of the object ~\cite{ObjDetWithDeepLearReview}. Object recognition is used in the automotive industry, video surveillance, robotics, and other fields ~\cite{ObjectDetectionIn20Years,AReviewObjectDetectionTechniques}.
 
% Object recognition is sometimes only the end product. Typically, detected objects are used as input data for various other methods, such as image segmentation, tracking, and image captioning ~\cite{ObjectDetectionIn20Years}.
% Over the past twenty years, object recognition technology has experienced rapid development. This development can be attributed to significant advances in machine learning. 
Over the past twenty years, object recognition technology has experienced rapid development. This development can be attributed to significant advances in machine learning. 
The work "Object Detection in 20 Years: A Survey" states that the development of object recognition can be divided into two periods: before and after the use of deep machine learning. The methods of the first period are called "\emph{traditional object recognition methods}", while those of the second period are called "\emph{deep learning-based object detection}".~\cite{ObjectDetectionIn20Years}

Traditional object recognition methods include:
\begin{enumerate}
    \item Viola-Jones Detectors
    \item HOG Detector
    \item Deformable Part-Based Model~\cite{ObjectDetectionIn20Years}
\end{enumerate}

Viola-Jones detectors were invented in 2001 by P. Viola and M. Jones. With this method, real-time human face recognition was achieved. As the authors note in their article "Rapid object detection using a boosted cascade of simple features", the main contribution of their discovery consists of three aspects:
\begin{enumerate}
    \item Integral image -- a new image representation that allows for fast feature evaluation.
    \item A method for building a classifier by selecting a small number of essential features using AdaBoost.
    \item A method for cascading classifier construction, which significantly accelerates the detection speed of an object in an image.~\cite{ViolaJones}
\end{enumerate}

The working principle of the algorithm is as follows. First, an integral image is generated on the basis of the pixel values of the image. The calculation is done only once, greatly accelerating the calculations. Then a sliding sub-window of 24 by 24 pixels is created (the size may vary), in which the required features are checked in a cascading manner; the example is shown in ~\ref{fig:face_features}.~\cite{ViolaJones}
\begin{figure}[h]
    \caption{Haar features, selected for face detection~\cite{990517figures} }
    \centering
    \includegraphics[width=.5\textwidth]{text/images/face_features.png}
    \label{fig:face_features}
\end{figure}
 

If the sub-window passes the first check, it is tested on the next classifier, and so on. If the sub-window does not pass the test on any classifier, that sub-window is discarded, and another sub-window is checked. This cascading classification method enables faster computation time, as most sub-windows will be discarded in the first and simpler classifiers, with more complex ones being in the last steps. If a sub-window passes through all classifiers, a positive result for face detection is produced, indicating that a face is present. The principle of cascading classifier operation is schematically shown in figure ~\ref{fig:cascade_detection}.~\cite{ViolaJones}

\begin{figure}[h]
    \caption{Visualization of cascade detection~\cite{990517figures}}
    \centering
    \includegraphics[width=.5\textwidth]{text/images/cascade_image.png}
    \label{fig:cascade_detection}
\end{figure}

At first glance, the working principle of the Viola-Jones detector is relatively simple. However, computing power that was considered immense at that time was required for the calculations.~\cite{ViolaJones}

\vspace{0.5cm}

The HOG Detector, which has been an essential component of numerous object detection algorithms over the years, was introduced by Dalal and Triggs in 2005 to enhance pedestrian detection methods. Although the main objective of its development was the detection of people, it can also be used to detect other objects.~\cite{ObjectDetectionIn20Years}

The working principle is schematically illustrated in Figure ~\ref{fig:hog_detection}. "\emph{The method is based on evaluating well-normalized local histograms of image gradient orientations in a dense grid. The fundamental idea is that the appearance and shape of a local object can often be characterized relatively well by the distribution of local intensity gradients or edge directions, even without precise knowledge of the corresponding gradient or edge positions}"~\cite{hogDetection}.
In practice, the detection process unfolds as follows:
\begin{enumerate}
    \item A detection window, typically 64x128 pixels in size (which can be adjusted for smaller images), is selected.
    \item This window traverses all possible parts and scales of the image during the detection process.

    \item The gradient is calculated for each pixel within the given window.
    \item The image is divided into 8x8-pixel blocks and a 9-bin histogram is computed for each of them.
Subsequently, cells are grouped into 2x2, 4x4, and 8x8 blocks.
    \item On the basis of the blocks obtained, a feature vector is computed, which is then normalized and combined with other vectors to create the HOG feature.
    \item Finally, the HOG features are fed into a linear SVM classifier, which responds to whether a human is present in the image~\cite{hogDetection}.
\end{enumerate}

\begin{figure}[h]
    \caption{An overview of our feature extraction and object detection chain~\cite{1467360figures} }
    \centering
    \includegraphics[width=1\textwidth]{text/images/1467360-fig-1-source-large.png}
    \label{fig:hog_detection}
\end{figure}

The Deformable Part-Based Model (DPM) was proposed by Felzenszwalb in 2008 and embodies traditional object detection methods and extends the HOG detector. A vital feature of this method is its "divide and conquer" approach, in which object detection is divided into several smaller tasks. For example, one could first detect individual body parts such as the head, arms, and legs to detect a person. Upon detecting all these parts, it is then possible to accurately determine whether a person is present in the image, as illustrated in Figure ~\ref{fig:dpm_detection}.~\cite{ObjectDetectionIn20Years, DPMdetection}

\begin{figure}[h]
    \caption{Example detection obtained with the person model ~\cite{4587597figures} }
    \centering
    \includegraphics[width=0.8\textwidth]{text/images/DPMexample.png}
    \label{fig:dpm_detection}
\end{figure}

The detection system employs a sliding-window principle. The object model consists of a primary filter and part models. Each part model includes a spatial model, which indicates where a specific part of the object may be located, and a corresponding filter.
Simplified object detection can be divided into the following steps:
\begin{enumerate}
    \item A feature map is constructed using the HOG method.
    \item The sliding window traverses each region of the feature map, and the filters are used to check for template matches. The primary template represents the entire object, while the others represent object parts.
    \item For each region, a detection score is calculated based on the primary and auxiliary filters.
    \item The detection result is determined based on the detection score.~\cite{DPMdetection}

\end{enumerate}

Although most modern object detection methods surpass DPM in terms of accuracy, many incorporate the principles presented within them. In 2010, the authors of the method received a "lifetime achievement" award from PASCAL VOC for their significant contributions to the field.~\cite{ObjectDetectionIn20Years}







