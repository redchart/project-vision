
%---------------------------------------------------------------
\chapter{Creazyflie platform}
%---------------------------------------------------------------

\section{Description}

Crazyflie is a versatile open source platform created by Bitcraze in 2011. It originated from a simple idea to make a board fly. Three embedded systems engineers envisioned a small machine with minimal components that would be suitable for indoor use. Today, Crazyflie is a palm-sized platform, making it ideal for education, research, and swarm robotics.~\cite{bitcraze_system_overview,bitcraze_about}

For a better understanding, the author suggests examining Figure ~\ref{fig:cf_eco_system}, which shows a schematic representation of the CrazyFlie platform ecosystem. As can be seen, this ecosystem consists of three groups of devices and software. The first group includes the CrazyFlie 2.1 nano quadcopter, the Crazyflie Bolt 1.1 quadcopter controller, and the Roadrunner UWB positioning tag. Each device has advantages and disadvantages, but they are built on similar principles and can interact with components from other groups. The second group comprises the Python client, which runs on the user's local computer and communicates with devices through the Crazyradio PA radio transmitter. Finally, the third group consists of positioning systems, namely the Lighthouse Positioning System and the Loco Positioning System.~\cite{bitcraze_system_overview,bitcraze_about}

\begin{figure}[h]
    \caption{Crazyflie 2.1 without modules~\cite{Bitcraze21figures} }
    \centering
    \includegraphics[width=.75\textwidth]{text/images/system_overview.jpg}
    \label{fig:cf_eco_system}
\end{figure}

\section{Crazyflie 2.1 quadcopter}

In this bachelor's thesis, the author uses the Crazyflie 2.1 quadcopter. This drone is notable for its take-off weight of 27 grams and small dimensions of 92x92x29mm (motor-to-motor and including motor mount feet). In addition, its size and weight allow safe use in small indoor spaces, such as the Robotic Agents Laboratory (RoboAgeLab), which has a designated safe flight zone.~\cite{bitcraze_crazyflie_2_1_datasheet}

\begin{figure}[h]
    \caption{Crazyflie 2.1 without modules~\cite{Bitcraze21figures} }
    \centering
    \includegraphics[width=.5\textwidth]{text/images/crazyflie_2.1_585px.jpg}
    \label{fig:cf_without_modules}
\end{figure}

Crazyflie 2.1 has a durable construction capable of withstand falls and collisions during experiments. Additionally, it is easy to assemble and requires no soldering, making it accessible to people without experience with embedded systems. The software is loaded wirelessly, offering convenience and speed in software development. The quadcopter also has an on-board charging feature. Another useful feature is real-time logging, graphing, and variable setting, in addition to the full use of expansion decks when using a Crazyradio or Crazyradio PA and a computer.~\cite{bitcraze_crazyflie_2_1_datasheet}

The Crazyflie 2.1 board has a dual MCU architecture and features the following onboard microcontrollers:

\begin{enumerate}
    \item STM32F405 main application MCU (Cortex-M4, 168MHz, 192kb SRAM, 1Mb flash);
    \item nRF51822 radio and power management MCU (Cortex-M0, 32Mhz, 16kb SRAM, 128kb flash);
    \item micro-USB connector;
    \item on-board LiPo charger with 100mA, 500mA, and 980mA modes available;
    \item full-speed USB device interface;
    \item partial USB OTG capability (USB OTG present but no 5V output);
    \item 8KB EEPROM.~\cite{bitcraze_crazyflie_2_1_datasheet}
\end{enumerate}

 Crazyflie 2.1 has a battery that provides a flight time of 7 minutes and takes approximately 40 minutes to recharge. The drone is able to receive radio signals at a frequency of 2.4GHz and a range of up to 1 kilometer using Crazyradio PA, broadening the spectrum of tasks it can perform. In addition, the drone can carry at least 15 grams of weight, allowing for the placement of additional modules such as the Lighthouse Positioning Deck, Loco Positioning Deck, Flow Deck V2, AI-Deck, and others. The author will use the Loco Positioning Deck and the AI-Deck in this thesis.~\cite{bitcraze_crazyflie_2_1_datasheet}
 % [https://www.bitcraze.io/documentation/hardware/crazyflie_2_1/crazyflie_2_1-datasheet.pdf]

\section{AI-deck module}

The AI-deck 1.1, designed for AI on the edge purposes, is built around the GAP8 RISC-V multicore MCU.
In addition, the QVGA monochrome camera and ESP32 WiFi MCU, a combination of which creates a pretty good platform to develop low-power AI on edge for a drone, are also available on the deck.
The AI-deck 1.1 extends the computational capacity of the GAP8 and enables the implementation of complex workloads based on artificial intelligence that is driven onboard and can achieve fully autonomous navigation capabilities.~\cite{bitcraze_ai_deck_1_1_datasheet}

\begin{figure}[h]
    \caption{AI-deck 1.1 and Crazyflie 2.1 with installed AI-deck module~\cite{BitcrazeStorefigures} }
    \centering
    \subfloat[AI-deck 1.1 module]{
        \includegraphics[width=.3\textwidth]{text/images/aideck.png}
    }
    \subfloat[Crazyflie 2.1 with installed AI-deck module]{
        \includegraphics[width=0.6\textwidth]{text/images/aideckwithDrone.jpeg}
    }
    
    \label{fig:ai_deck}
\end{figure}

The ESP32 adds WiFi connectivity with the possibility to stream images and handle control. The developers of the AI-deck believe that this lightweight and low-power combination opens up many research and development areas for the microsized Crazyflie 2.X UAV.~\cite{bitcraze_ai_deck_1_1_datasheet}

% [https://www.bitcraze.io/documentation/hardware/ai_deck_1_1/ai_deck_1_1-datasheet.pdf]

\subsection{GAP8 processor}

GAP8 is an IoT application processor that enables the massive implementation of low-cost battery-operated intelligent devices that capture, analyze, classify, and react to the combined flow of rich data sources such as images, sounds, radar signatures, and vibrations. 
GAP8 is specifically optimized to perform a wide range of image and audio algorithms, including convolutional neural network inference and signal processing, with extraordinary energy efficiency.
GAP8 allows manufacturers of industrial and consumer products to integrate signal processing, artificial intelligence, and advanced classification into new classes of battery-operated wireless edge devices for IoT applications, including image recognition, person- and object-counting, machine health monitoring, home security, speech recognition, audio enhancement, consumer robotics and smart toys, as well as intelligent gadgets.
By enabling autonomous operation, GAP8 drastically reduces the cost of deployment and operation of a wide range of smart edge devices.~\cite{greenwaves_gap8_sheet, greenwaves_gap8_manual}


The hierarchical and demand-driven architecture architecture of the GAP8 processor
enables ultralow power operation by combining:
\begin{enumerate}
    \item A collection of highly autonomous intelligent I/O
        peripherals for connection to cameras,
        microphones and other capture and control
        devices.
    \item A fabric controller (FC) core for control,communications and security functions.
    \item A compute cluster of 8 cores.
    \item A Convolutional Neural Network accelerator(HWCE).~\cite{greenwaves_gap8_sheet, greenwaves_gap8_manual}
\end{enumerate}


\begin{figure}[h]
    \caption{GAP8 Block Diagram~\cite{greenwaves_gap8_manualFigures} }
    \centering
    \includegraphics[width=1.0\textwidth]{text/images/GAP8Layout.png}
    \label{fig:gap_block_diagram}
\end{figure}

The GAP8 processor has nine cores, eight working in parallel in a cluster for high-performance computations, and a single "Fabric Controller" (FC) core for managing operations. The processor supports an extended RISC-V instruction set and has specialized instructions for optimizing targeted algorithms. It has a two-level memory structure: 512KB Level 2 memory accessible to all processors and DMA units and smaller Level 1 memory for the FC (16KB) and cluster cores (64KB). There is also access to external memory areas through HyperBus or quad-SPI peripherals. Internal memory is preferred over external memory access to optimize energy consumption and performance. The processor's capabilities quickly adapt to various applications' processing and energy requirements, making it suitable for tasks such as image processing, audio processing, and signal modulation.~\cite{greenwaves_gap8_sheet, greenwaves_gap8_manual}


% [https://greenwaves-technologies.com/manuals/BUILD/HOME/html/index.html, https://greenwaves-technologies.com/wp-content/uploads/2021/04/Product-Brief-GAP8-V1_9.pdf]


\subsection{Camera}

The HM01B0 is an ultralow power CMOS Image Sensor that enables the integration of an “Always On” camera for computer vision applications such as gestures, intelligent ambient light and proximity sensing, tracking and object identification. The unique architecture of the sensor allows the sensor to consume a very low power of 2mW at QVGA 30FPS.~\cite{himax_web,himax_manual}

The HM01B0 sensor has the following parameters:
\begin{enumerate}
    \item Active Pixel Array -- 320 x 320
    \item Pixel Size -- 3.6 µm x 3.6 µm
    \item Full Image Area -- 1152 µm x 1152 µm
    \item Diagonal (Optical Format)	-- 1.63 mm (1/11")
    \item Color Filter Array -- Monochrome and Bayer
    \item Effective Focal Length -- 0.66 mm~\cite{himax_web,himax_manual}
\end{enumerate}

The HM01B0 sensor supports a window mode of 320 by 240, allowing for 60 frames per second. Additionally, a 2x2 binning can achieve a speed of 120 FPS. Monochrome video is transmitted through a 1, 2, or 8-bit interface. In order to optimize the power consumption, the HM01B0 sensor integrates a black-level calibration scheme, an automatic exposure and gain control loop, an auto-generator, and a motion detection circuit with an interrupt output to reduce host computations and sensor commands. Taking into account the parameters of this camera, it fits IoT, wearable devices, smart buildings, smartphones, tablets, and slim notebooks.~\cite{himax_web,himax_manual}

% [https://www.himax.com.tw/products/cmos-image-sensor/always-on-vision-sensors/hm01b0/]


\section{Crazyradio PA}

The Crazyradio PA is a "\emph{long-range open USB radio dongle based on the nRF24LU1+ from Nordic Semiconductor. It features a 20dBm power amplifier, LNA, and comes pre-programmed with Crazyflie compatible firmware.}" Crazyradio PA is depicted in figure ~\ref{fig:crazyradio} ~\cite{bitcraze2022crazyradio}.
Key features include:
\begin{enumerate}
    \item Radio power amplifier providing 20dBm output power
    \item Over 1km line-of-sight range with Crazyflie 2.X
    \item Low latency~\cite{bitcraze2022crazyradio}
\end{enumerate}


\begin{figure}[h]
    \caption{Crazyradio PA~\cite{BitcrazePAfigures}}
    \centering
    \includegraphics[width=.3\textwidth]{text/images/Radio-PA-585px.jpeg}
    \label{fig:crazyradio}
\end{figure}

In this work, the Crazyradio PA is utilized to communicate with CrazyFlie 2.1 and to install and update the software on the AI-deck 1.1 module. The Crazyradio PA connects to the computer via USB, offering 125 radio channels and the capability to transmit information at speeds of up to 2Mbps.~\cite{bitcraze2022crazyradio}

\section{Loco positioning module}

The Loco Positioning system is a local positioning system based on Ultra Wide Band radio, which determines the absolute 3D position of objects in space. In many ways, it is similar to a miniature GPS system. The example is shown in figure ~\ref{fig:loco_pos_example}.~\cite{bitcraze_loco_positioning,bitcraze2020loco}

\begin{figure}[h]
    \caption{The Loco Positioning system~\cite{BitcrazeLocoPosfigures} }
    \centering
    \includegraphics[width=.8\textwidth]{text/images/lpssystem.png}
    \label{fig:loco_pos_example}
\end{figure}

The key components of the Loco Positioning system are Anchors and Tags. Anchors are placed within the indoor environment and serve as reference points, similar to satellites in GPS systems. Tags are placed on the object whose position we want to track. All information about the object's position is located on the tag, which, unlike other systems, does not require additional communication with other devices. As a result, Loco Positioning expands the autonomy capabilities of the Crazyflie 2.1 drone. In this thesis, the author uses a loco positioning set-up with eight anchors and one tag -- loco positioning deck placed on the Crazyflie 2.1. The enchor setup is shown in Figure ~\ref{fig:lab_loco_setup}.~\cite{bitcraze_loco_positioning,bitcraze2020loco}

\begin{figure}[h]
    \caption{Loco positioning deck and Crazyflie 2.1 with installed AI-deck module~\cite{BitcrazeLocoPos2figures} }
    \centering
    \subfloat[Loco positioning module]{
        \includegraphics[width=.2\textwidth]{text/images/locoPosDeck.jpeg}
    }
    \subfloat[Crazyflie 2.1 with installed Loco positioning module]{
        \includegraphics[width=0.4\textwidth]{text/images/locoModule.jpeg}
    }
    
    \label{fig:ai_deck}
\end{figure}


\begin{figure}[h]
    \caption{8 anchor loco positioning setup from the lab. Anchors are outlined with red circles}
    \centering
    \includegraphics[width=1.0\textwidth]{text/images/labLocoReal.png}
    \label{fig:lab_loco_setup}
\end{figure}

\let\cleardoublepage=\clearpage