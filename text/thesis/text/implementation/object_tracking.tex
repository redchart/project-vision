This document describes the steps involved in object tracking using the Kalman Filter and the choice of parameters used in the given code. The code provided is implemented in Python and utilizes the OpenCV library.

The object tracking process using the Kalman filter consists of the following steps:

\begin{enumerate}
    \item Initialize the Kalman Filter with appropriate parameters.
    \item Convert the initial bounding box of the object to a state vector.
    \item Predict the next state using the Kalman Filter.
    \item Update the state with the new measurement.
    \item Convert the current state to a bounding box.
\end{enumerate}

The transition matrix, denoted as $A$, is an $8\times8$ matrix that models the relationship between the current state and the next state. The matrix $A$ contains values that describe the relationships between the state variables, which include the center coordinates of the bounding box $(x, y)$, the width and height $(w, h)$, and their respective velocities $(v_x, v_y, v_w, v_h)$. The diagonal elements of the matrix are set to one, indicating that the state variables are directly carried over from one time step to the next. Meanwhile, the off-diagonal elements in the first four rows represent how velocities contribute to the position and size of the bounding box. $A$ is defined as follows:

\begin{equation}
A = \begin{bmatrix}
1 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 1 \\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1
\end{bmatrix}
\end{equation}

The measurement matrix, denoted $H$, is a $4\times8$ matrix that maps the state vector to the measurement vector. The matrix $H$ shows how the observed variables relate to the state variables. The variables observed in this case are the center coordinates of the bounding box $(x, y)$, as well as the width and height $(w, h)$. The diagonal elements of the matrix have a value of one, indicating that the observed variables correspond directly to the state variables. On the other hand, zero elements in the matrix mean that velocities $(v_x, v_y, v_w, v_h)$ do not directly affect the observed variables. $H$ is defined as follows:

\begin{equation}
H = \begin{bmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0
\end{bmatrix}
\end{equation}

The process noise covariance matrix, denoted as $Q$, is an $8\times8$ matrix that models the uncertainty in the state transition. The choice of the process noise covariance matrix in this code is as follows:

\begin{equation}
Q = 5 \cdot I_{8}
\end{equation}

The measurement noise covariance matrix, denoted as $R$, is a $4\times4$ matrix that models the uncertainty in the measurements. The choice of the measurement noise covariance matrix in this code is as follows:

\begin{equation}
R = 1 \times 10^{-5} \cdot I_{4}
\end{equation}

Where $I_{4}$ and $I_{8}$ are identity matrices of size $4\times4$ and $8\times8$, respectively. Identity matrices are square matrices with ones on the diagonal and zeros elsewhere. They have the property that, when multiplied by another matrix, they do not change the other matrix.

In this context, $I_{4}$ and $I_{8}$ are defined as follows:

\begin{align}
I_{4} &= \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
0 & 0 & 0 & 1
\end{bmatrix},
&
I_{8} &= \begin{bmatrix}
1 & 0 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 1 & 0 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 1 & 0 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 1 & 0 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 1 & 0 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 1 & 0 \\
0 & 0 & 0 & 0 & 0 & 0 & 0 & 1
\end{bmatrix}
\end{align}

$I_{8}$ and $I_{4}$ are used as a base for creating the process noise covariance matrix $Q$ and the measurement noise covariance matrix $R$.

The values chosen for the process noise covariance matrix and measurement noise covariance matrix have a significant impact on how well the Kalman filter can track an object. These parameters can be adjusted to balance the filter's responsiveness to state changes and noise reduction in the state estimates. For instance, increasing the process noise covariance values can make the filter more responsive to sudden changes in the object's state, but it may also introduce more noise in the estimates. Conversely, decreasing the measurement noise covariance values can increase the filter's reliance on measurements, which could lead to overfitting the observed data and reduced smoothness in the state estimates. The code of the Kalman filter implementation can be found in Appendix ~\ref{kalmanfilter}.



