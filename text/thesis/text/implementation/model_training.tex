To train and test the SSD MobileNet V2 FPNLite 320x320 model, the author utilized the Google Colab online service. This decision was made due to its ability to utilize external computational resources, significantly speeding up the model training process. In addition, Colab offers an environment where Jupyter notebooks can be executed. 

The author created a Jupyter notebook called "teddy\_ssd\_mobilenet\_v2\_fnlite.ipynb," which contains step-by-step instructions for training, testing, and exporting the object detection model for a teddy bear. In the following text, the author will outline several crucial steps of the created notebook. The code of the entire "teddy\_ssd\_mobilenet\_v2\_fnlite.ipynb" notebook can be found in Appendix ~\ref{teddy_yupyter}.

The author used and modified the existing Jupyter notebook ~\cite{mehta2021objectdetection}. The author used the TensorFlow2 library to train the model. To begin with, they installed all the required libraries and dependencies at the beginning of the notebook. Subsequently, training data was prepared by generating CSV files that included details about the images, such as filename, width, height, class, xmin, ymin, xmax, and ymax. These CSV files were converted to tf record files, an effective file format for storing large volumes of data for machine learning purposes. Lastly, the author fine-tuned the hyperparameters as follows:
\begin{listing}\caption{Setting a hyperparameters}\label{python}
    \begin{minted}{python}
    batch_size = 16
    num_steps = 50000
    num_eval_steps = 1000
    \end{minted}
\end{listing}

\begin{enumerate}
    \item batch\_size -- The batch size is the number of training samples used in each update of the model weights during training. A smaller batch size means that the model will update its weights more frequently, while a larger batch size will result in less frequent updates. The size of the batch can affect the speed of training and the stability of convergence. In this case, the batch size is set to 16, which means that the model will use 16 samples in each update.
    \item num\_steps -- This hyperparameter specifies the total number of steps (or iterations) that the training process will take. Each step consists of processing one batch of data and updating the model weights accordingly. In this case, the model will be trained for 50,000 steps before stopping. Keep in mind that this is different from epochs, which refer to the number of times the model goes through the entire training dataset.
    \item num\_eval\_steps -- This hyperparameter specifies the number of steps used to evaluate the model during the training process. Evaluation is typically done on a separate dataset, known as the validation or evaluation dataset, to measure the model's performance on unseen data. In this case, the model will be evaluated every 1,000 steps. This allows you to monitor the model's performance during training and identify potential issues such as overfitting.
\end{enumerate}

After adjusting the hyperparameters, the script is executed to train the model with the input provided. In the next section, the author will present and clarify the training results using graphs obtained during the training process.

The learning rate is a key element in training a model that decides how much the model's weights are updated. A higher learning rate means that the model will make more significant adjustments, whereas a lower learning rate will result in more minor adjustments. Figure ~\ref{fig:learning_and_classification_a} shows a graph that illustrates the data from the training process, where the learning rate starts at 0.08 and decreases to 0 after 50,000 steps. This approach is commonly referred to as learning rate scheduling, which helps the model to converge to the best possible solution by gradually decreasing the learning rate over time.
\begin{figure}[h]
    \centering
    \subfloat[Learning rate graph\label{fig:learning_and_classification_a}]{
        \includegraphics[width=0.45\textwidth]{text/images/learning_rate.jpg}
    }
    \hfill
    \subfloat[Classification loss graph\label{fig:learning_and_classification_b}]{
        \includegraphics[width=0.45\textwidth]{text/images/classification_loss.jpg}
    }
    
    \caption{Training graphs}
\end{figure}

The classification loss measures the accuracy of the model in predicting the class labels of the input samples. The graph in Figure ~\ref{fig:learning_and_classification_b} shows that the classification loss decreases from 0.11 to 0.0016 during training, indicating that the model's ability to classify the data has improved greatly.

When predicting the location of objects in input data, there is an associated error called localization loss. This is especially important in object detection and segmentation tasks, where the model must determine the object's class label and position in the input image. The graph in Figure ~\ref{fig:localization_and_total_a} shows that the loss of localization starts at 0.11 and decreases to 0.0018 throughout the training process, indicating an improvement in the model's ability to locate objects.
\begin{figure}[h]
    \centering
    \subfloat[Learning rate graph\label{fig:localization_and_total_a}]{
        \includegraphics[width=0.45\textwidth]{text/images/localization_loss.jpg}
    }
    \hfill
    \subfloat[Classification loss graph\label{fig:localization_and_total_b}]{
        \includegraphics[width=0.45\textwidth]{text/images/total_loss.jpg}
    }
    
    \caption{Training graphs}
\end{figure}

To avoid overfitting, regularization loss is incorporated into the loss function to penalize too complex models. This involves adding a penalty term based on the model's weights. As shown in Figure ~\ref{fig:localization_and_total_b}, the regularization loss graph starts at 0.15 and gradually decreases to 0.04 during training, indicating a decrease in the complexity of the model.

The total loss combines the classification, localization, and regularization loss. The primary goal of the training process is to reduce this overall goal. The graph in Figure 1 shows the total loss decreasing from 0.396 to 0.054, which means that the model has successfully learned to reduce the error in the training data.

The author gained valuable insights into the training process by analyzing these graphs. They show how the model's performance in different areas, such as classification, localization, and complexity, changes over time as it learns from the training data. The fact that the classification loss, localization loss, and total loss values decrease indicates that the model is effectively learning from the data. However, the decreasing learning rate and loss of regularization suggest that model complexity is being regulated to avoid overfitting.
