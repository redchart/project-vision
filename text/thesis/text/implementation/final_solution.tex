The individual building blocks were combined into a single script called flying\_streamer.py, which can be found in Appendix ~\ref{flyingstreamer}. In the following text, the author will explain how this script works.

Initially, all necessary libraries and dependencies were imported. Subsequently, the object detection model was initialized. Then, the connection to the drone and the AI-deck was set up. Subsequently, the drone was given the command to take off and move to the starting point to start an object search. Once the drone reached the designated position, the object detection process was started. Upon successful detection, object tracking and distance estimation were performed. It is worth noting that if the object was not detected in the last ten frames, the drone would change its orientation and attempt to detect the object again.

\begin{figure}[h]
    \centering
    \subfloat{
        \includegraphics[width=0.3\textwidth]{text/images/detection1.png}
    }
    \hfill
    \subfloat{
        \includegraphics[width=0.3\textwidth]{text/images/detection2.png}
    }
    \hfill
     \subfloat{
        \includegraphics[width=0.3\textwidth]{text/images/detection3.png}
    }
    \caption{Result images}
    \label{fig:detectionimages}
\end{figure}

As a result of the testing under laboratory conditions, images were obtained, which can be seen in Figure ~\ref{fig:detectionimages}. Upon successful detection, the object is displayed within a green bounding box. Above the bounding box, the detected class name is displayed; in our case, it is "teddy", along with the detection accuracy. A red rectangle represents the object tracking prediction. Finally, the distance estimation is printed inside the bounding box.

The testing provided helpful information to analyze the precision and correctness of detection, tracking, and distance estimation from the object to the drone. The author will now describe and analyze each of the graphs created from the obtained data.

\begin{figure}[h]
    \caption{Detection success rate graph}
    \centering
    \includegraphics[width=0.7\textwidth]{text/images/det_acc.jpg}
    \label{fig:det_acc}
\end{figure}

Figure ~\ref{fig:det_acc} shows the object detection success rate graph relative to the number of frames at a given moment. For example, the graph will display the percentage of successful detection over the previous ten frames at the point for the tenth frame. This graph shows that the object was detected in most frames. However, it also highlights segments where the detection rate drops sharply, indicating that the object was out of the drone's field of view during those segments.


\begin{figure}[h]
    \caption{Detection accuracy graph}
    \centering
    \includegraphics[width=0.7\textwidth]{text/images/det_acc_for_succ_det.jpg}
    \label{fig:det_acc_for_succ_det}
\end{figure}

Figure ~\ref{fig:det_acc_for_succ_det} presents the object detection accuracy graph. Upon analyzing this graph, the author concludes that the object detection accuracy has excellent accuracy and very low variation.


\begin{figure}[h]
    \caption{Tracking accuracy graph}
    \centering
    \includegraphics[width=0.7\textwidth]{text/images/iou_tracking.jpg}
    \label{fig:iou_tracking}
\end{figure}

Figure ~\ref{fig:iou_tracking} displays the tracking algorithm's success rate graph based on the Intersection over Union (IOU) metric. IOU measures the degree of overlap between the bounding box created during detection and the predicted bounding box. The values of this metric range from 0 to 1. Low values indicate that the object position prediction algorithm is inaccurate, while high values indicate high accuracy. Figure ~\ref{fig:iou_tracking} shows that the prediction accuracy varies between 0.5 and 0.9, which the author believes is a good result. It is worth noting that the shallow values on the graph are associated with a sudden change in the object's motion. After a low value, there is always a significant increase, indicating that the algorithm quickly adapts.

\begin{figure}[h]
    \caption{Distance estimation accuracy graph}
    \centering
    \includegraphics[width=0.7\textwidth]{text/images/distance_meas.jpg}
    \label{fig:distance_meas}
\end{figure}

Figure ~\ref{fig:distance_meas} shows two graphs -- the first represents the actual distance of the object from the camera, and the second represents the distance calculated by the algorithm. It should be noted that the actual distance may have errors due to drone flight instability. After analyzing the graphs, the author concludes that there is a systematic error in distance measurement, leading to nearly 20 percent inaccuracy. In the author's opinion, this error arises from a combination of factors, such as inaccuracies in bounding box dimensions and the drone's motion.

The average values of detection, detection accuracy, IOU and distance estimation are presented in Table ~\ref{tab:average_values}. The data in the table demonstrate that, overall, the object detection, tracking, and distance estimation algorithms perform with good accuracy.
\begin{table}[h]
\centering
\begin{tabular}{|l|c|}
\hline
\textbf{Metric} & \textbf{Average Value in \%} \\
\hline
Detection & \detokenize{87.21} \\
\hline
Detection Accuracy & \detokenize{99.78} \\
\hline
Intersection over Union (IOU) & \detokenize{74.85} \\
\hline
Average distance estimation error & \detokenize{19.62} \\
\hline
\end{tabular}
\caption{Average values of detection, detection accuracy, IOU and distance estimation}
\label{tab:average_values}
\end{table}
