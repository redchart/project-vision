import cv2
import numpy as np

# Initialize the Kalman Filter
def initialize_kalman_filter():
    kf = cv2.KalmanFilter(8, 4)
    kf.errorCovPost = np.eye(8, dtype=np.float32) * 1e2

    kf.transitionMatrix = np.array([
        [1, 0, 0, 0, 1, 0, 0, 0],
        [0, 1, 0, 0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0, 0, 1, 0],
        [0, 0, 0, 1, 0, 0, 0, 1],
        [0, 0, 0, 0, 1, 0, 0, 0],
        [0, 0, 0, 0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0, 0, 1, 0],
        [0, 0, 0, 0, 0, 0, 0, 1]], np.float32)

    kf.measurementMatrix = np.array([
        [1, 0, 0, 0, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0, 0, 0, 0]], np.float32)

    # Define process and measurement noise covariance matrices
    kf.processNoiseCov = np.eye(8, dtype=np.float32) * 5
    kf.measurementNoiseCov = np.eye(4, dtype=np.float32) * 1e-5

    return kf

# Convert the bounding box to a state vector
def bbox_to_state(bbox):
    x, y, w, h = bbox
    return np.array([x+w/2, y+h/2, w, h, 0, 0, 0, 0], dtype=np.float32)

def bbox_to_state_oof(bbox):
    x, y, w, h = bbox
    return np.array([x+w/2, y+h/2, w, h], dtype=np.float32)

# Convert the state vector to a bounding box
def state_to_bbox(state):
    x, y, w, h, _, _, _, _ = state
    return (int(x-w/2), int(y-h/2), int(w), int(h))
