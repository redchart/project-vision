import time
import math
from cflib.positioning.position_hl_commander import PositionHlCommander

class PositionExtendedCommander(PositionHlCommander):
    # seconds for 1 degree
    TURNING_SPEED = 0.03
    DEFAULT = None

    def __init__(self, crazyflie,
                 x=0.0, y=0.0, z=0.0, yaw=0,
                 default_velocity=0.5,
                 default_height=0.5,
                 controller=None,
                 default_landing_height=0.0):
        super().__init__(crazyflie,
                         x=x, y=y, z=z,
                         default_velocity=default_velocity,
                         default_height=default_height,
                         controller=controller,
                         default_landing_height=default_landing_height)
        self._yaw = yaw

    def __rotate(self, yaw_angle_degrees):
        x = self._x
        y = self._y
        z = self._z
        yaw = self._yaw + math.radians(yaw_angle_degrees)

        duration_s = math.fabs(yaw_angle_degrees * self.TURNING_SPEED)
        self._hl_commander.go_to(x, y, z, yaw, duration_s, False)
        time.sleep(duration_s)
        self._yaw = yaw

    def turn_left(self, angle):
        self.__rotate(math.fabs(angle))

    def turn_right(self, angle):
        self.__rotate(-1 * angle)

    def left(self, distance_m, velocity=DEFAULT):
        """
        Go left

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        perpendicular_angle = self._yaw + math.pi / 2
        x_component = math.cos(perpendicular_angle) * distance_m
        y_component = math.sin(perpendicular_angle) * distance_m
        self.move_distance(x_component, y_component, 0.0, velocity)

    def right(self, distance_m, velocity=DEFAULT):
        """
        Go right

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        perpendicular_angle = self._yaw + math.pi / 2
        x_component = math.cos(perpendicular_angle) * distance_m
        y_component = math.sin(perpendicular_angle) * distance_m
        self.move_distance(-x_component, -y_component, 0.0, velocity)

    def forward(self, distance_m, velocity=DEFAULT):
        """
        Go forward

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        dist_x = distance_m * math.cos(self._yaw)
        dist_y = distance_m * math.sin(self._yaw)
        self.move_distance(dist_x, dist_y, 0.0, velocity)

    def back(self, distance_m, velocity=DEFAULT):
        """
        Go backwards

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        dist_x = distance_m * math.cos(self._yaw)
        dist_y = distance_m * math.sin(self._yaw)
        self.move_distance(-dist_x, -dist_y, 0.0, velocity)

    def up(self, distance_m, velocity=DEFAULT):
        """
        Go up

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        self.move_distance(0.0, 0.0, distance_m, velocity)

    def down(self, distance_m, velocity=DEFAULT):
        """
        Go down

        :param distance_m: The distance to travel (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """
        self.move_distance(0.0, 0.0, -distance_m, velocity)

    def move_distance(self, distance_x_m, distance_y_m, distance_z_m,
                      velocity=DEFAULT):
        """
        Move in a straight line.
        positive X is forward
        positive Y is left
        positive Z is up

        :param distance_x_m: The distance to travel along the X-axis (meters)
        :param distance_y_m: The distance to travel along the Y-axis (meters)
        :param distance_z_m: The distance to travel along the Z-axis (meters)
        :param velocity: The velocity of the motion (meters/second)
        :return:
        """

        x = self._x + distance_x_m
        y = self._y + distance_y_m
        z = self._z + distance_z_m

        self.go_to(x, y, z, velocity)

    def go_to(self, x, y, z=DEFAULT, velocity=DEFAULT):
        """
        Go to a position

        :param x: X coordinate
        :param y: Y coordinate
        :param z: Z coordinate
        :param velocity: The velocity (meters/second)
        :return:
        """

        z = self._height(z)

        dx = x - self._x
        dy = y - self._y
        dz = z - self._z
        distance = math.sqrt(dx * dx + dy * dy + dz * dz)

        if distance > 0.0:
            duration_s = distance / self._velocity(velocity)
            self._hl_commander.go_to(x, y, z, self._yaw, duration_s)
            time.sleep(duration_s)

            self._x = x
            self._y = y
            self._z = z

