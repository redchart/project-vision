#!/usr/bin/env python3
import argparse
import socket, struct
import numpy as np
import libs.detection.detection as det
import libs.image_processing as ip
import libs.tracking.kalman as kalman
import time
import cflib.crtp
from cflib.crazyflie import Crazyflie
from cflib.crazyflie.syncCrazyflie import SyncCrazyflie
from libs.pos.pos_commander import PositionExtendedCommander
from cflib.utils import uri_helper

# Args for setting IP/port of AI-deck. Default settings are for when
# AI-deck is in AP mode.
parser = argparse.ArgumentParser(description='Connect to AI-deck JPEG streamer example')
parser.add_argument("-n",  default="192.168.4.1", metavar="ip", help="AI-deck IP")
parser.add_argument("-p", type=int, default='5000', metavar="port", help="AI-deck port")
parser.add_argument('--save', action='store_true', help="Save streamed images")
args = parser.parse_args()

deck_port = args.p
deck_ip = args.n

print("Connecting to socket on {}:{}...".format(deck_ip, deck_port))
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client_socket.connect((deck_ip, deck_port))
print("Socket connected")

imgdata = None
data_buffer = bytearray()

def rx_bytes(size):
    data = bytearray()
    while len(data) < size:
        data.extend(client_socket.recv(size-len(data)))
    return data

import cv2

start = time.time()
count = 0
exit_all = False

uri = uri_helper.uri_from_env(default='radio://0/80/2M/E7E7E7E7E5')
model = det.load_model('./inference_graph')
is_first = True
kf = kalman.initialize_kalman_filter()
cflib.crtp.init_drivers()

with SyncCrazyflie(uri, cf=Crazyflie(rw_cache='./cache')) as scf:
    last_x = 0.8
    last_y = 0.9
    last_z = -0.3
    with PositionExtendedCommander(
            scf,
            x=last_x, y=last_y, z=last_z, yaw=0,
            default_velocity=0.1,
            default_height=last_z + 0.5,
            controller=PositionExtendedCommander.CONTROLLER_PID,
            default_landing_height=last_z
    ) as pc:
        #go to init position
        pc.up(0.6)
        pc.right(0.9)

        frames_with_bear = 0
        frames_without_bear = 0
        #start object detection and tracking
        while(1):
            # First get the info
            packetInfoRaw = rx_bytes(4)
            [length, routing, function] = struct.unpack('<HBB', packetInfoRaw)

            imgHeader = rx_bytes(length - 2)
            [magic, width, height, depth, format, size] = struct.unpack('<BHHBBI', imgHeader)

            if magic == 0xBC:
                imgStream = bytearray()

                while len(imgStream) < size:
                    packetInfoRaw = rx_bytes(4)
                    [length, dst, src] = struct.unpack('<HBB', packetInfoRaw)
                    chunk = rx_bytes(length - 2)
                    imgStream.extend(chunk)

                bayer_img = np.frombuffer(imgStream, dtype=np.uint8)
                bayer_img.shape = (244, 324)
                color_img = cv2.cvtColor(bayer_img, cv2.COLOR_BayerBG2BGR)
                result_image = ip.process_image(
                    model=model,
                    image=color_img,
                    kf=kf,
                    is_first=is_first
                )

                if result_image is not None:
                    is_first = False
                    frames_with_bear += 1
                    cv2.imshow('result', result_image)
                else:
                    frames_without_bear += 1
                    if frames_without_bear > 10:
                        pc.turn_left(15)
                        frames_without_bear = 0

                    cv2.imshow('result', color_img)

                count += 1
                key = cv2.waitKey(1) & 0xFF
                if key == ord('s'):
                    break




