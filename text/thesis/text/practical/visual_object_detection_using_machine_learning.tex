
The author considered three factors to select the optimal object detection method for this work. The first factor is the detection speed. It is necessary to detect objects in real-time, specifically at the frame rate captured by the camera, which is 60 frames per second(FPS). The second factor is the accuracy of the detection. As the ultimate goal is to track the object after its detection, sufficient accuracy is required. The third factor is the mobility of the architecture, meaning that object detection should occur on a device with limited computational capabilities. Finally, a secondary factor is the ease of implementation and training of the detection model.

Based on all these criteria, the MobileNetV2-SSD model with FPNilte was chosen. Section ~\ref{subsec:machineLearning} described the SSD model that uses VGG-16 as the backbone model. Since this model requires significant storage space and has a relatively slower computation speed, the author decided to use the lighter and faster MobileNetV2 model. To achieve more accurate object detection, FPN was also employed. The schematic of the final architecture is shown in figure ~\ref{fig:Mobilenetv2SSD}.~\cite{Meng2022}

\begin{figure}[h]
    \caption{MobileNetV2-SSD architecture~\cite{GOSYSTECHfigures1}}
    \centering
    \includegraphics[width=1\textwidth]{text/images/Mobilenetv2SSDfinal.png}
    \label{fig:Mobilenetv2SSD}
\end{figure}

For a complete understanding of the chosen architecture, it is essential to comprehend how MobileNetV2 works. In the description of this network, the author relies on the work "MobileNetV2: Inverted Residuals and Linear Bottlenecks", which first introduced this network architecture. MobileNetV2 achieves high accuracy and computational speed due to three key factors:
\begin{enumerate}
    \item Depthwise Separable Convolutions
    \item Linear Bottlenecks
    \item Inverted Residuals~\cite{MobilenetV2}
\end{enumerate}

Depthwise Separable Convolutions are employed in various architectures because of their efficiency, which is why MobileNetV2's authors decided to utilize them when creating the new architecture. Its principle involves separating the full convolutional operator into two distinct layers. In the first, a convolutional filter is applied to each input channel, called depthwise convolution. In the second layer, a $1\times 1$ convolution calculates features "\emph{through computing linear combinations of the input channels}". Generally, to calculate one output tensor $L_{i}$, a convolutional filter $K \in \mathbb{R}^{k \times k \times d_i \times d_j}
$ is applied to an input tensor with dimensions $h_i \times w_i \times d_j
$. The number of operations is illustrated in:
\begin{equation}
     h_{i}\cdot w_{i}\cdot d_{i}\cdot d_{i}\cdot k\cdot k
\end{equation}
Using Depthwise Separable Convolutions can significantly reduce the number of computational operations, specifically:
\begin{equation}
     h_{i}\cdot w_{i}\cdot d_{i}(k^{2}+d_{j})
\end{equation}
"\emph{Effectively depthwise separable convolution reduces computation compared to traditional layers by almost a factor of $k^{2}$.}" By using a filter size of 3, the number of computational operations can be reduced by 8 to 9 times at the expense of a slight decrease in accuracy.~\cite{MobilenetV2}

In their work, the authors discuss the properties of activation tensors in deep neural networks and how they form a "manifold of interest" that can be embedded in low-dimensional subspaces. They highlight the benefits of using linear bottleneck layers in convolutional blocks to optimize neural architectures, as reducing dimensionality can improve efficiency and accuracy. However, the authors also note that non-linear transformations like ReLU can result in information loss and limit the network's classification capabilities. They highlight that ReLU can maintain complete information about the input manifold only if it lies in a low-dimensional subspace of the input space. Based on these insights, the authors suggest using linear bottleneck layers in convolutional blocks to maintain information while introducing complexity into the set of expressible functions.~\cite{MobilenetV2} 

Bottleneck blocks are similar to residual blocks, which have an input, several bottlenecks, and then an expansion. However, based on the idea that bottlenecks have all the needed information and the expansion layer is just a supporting detail for changing the tensor, the authors connected the bottlenecks directly with shortcuts. Figure ~\ref{fig:mobilenetInversedResiduals} shows the differences between the designs. The reason for adding shortcuts is similar to why we use regular residual connections: to help gradients flow through many layers. The inverted design saves more memory and works a bit better in our experiments.~\cite{MobilenetV2}

\begin{figure}[h]
    \caption{The difference between residual block and inverted residual~\cite{8578572figures}}
    \centering
    \includegraphics[width=0.8\textwidth]{text/images/mobilenetInversedResiduals.png}
    \label{fig:mobilenetInversedResiduals}
\end{figure}

After describing the theory and principles of operation of this model, the author outlines the architecture of the MobileNetV2 model. The authors of MobileNetV2 say "\emph{the basic building block is a bottleneck depth-separable convolution with residuals.}" ~\ref{fig:mobilenetTable1} displays the specific structure of this block. The MobileNetV2 architecture consists of an initial full convolution layer with 32 filters, followed by 19 residual bottleneck layers outlined in ~\ref{fig:mobilenetTable2}. ReLU6 is employed as the non-linearity due to its stability in low-precision calculations. The network authors consistently use a 3 × 3 kernel size, typical in contemporary networks, and apply dropout and batch normalization during training.~\cite{MobilenetV2}

\begin{figure}[h]
    \caption{Bottleneck residual block transforming from $k$ to $k^{\prime}$ channels, with stride $s$, and expansion factor $t$~\cite{8578572figures} }
    \centering
    \includegraphics[width=0.5\textwidth]{text/images/mobilenetTable1.png}
    \label{fig:mobilenetTable1}
\end{figure}
\begin{figure}[h]
    \caption{The architecture of MobileNetV2~\cite{8578572figures} }
    \centering
    \includegraphics[width=0.5\textwidth]{text/images/mobilenetTable2.png}
    \label{fig:mobilenetTable2}
\end{figure}

The authors of MobileNetV2 managed to create a straightforward network architecture that can then be used to create other models. This was achieved through the building unit, which, thanks to its properties, can work efficiently on devices with limited computational capabilities.~\cite{MobilenetV2}
