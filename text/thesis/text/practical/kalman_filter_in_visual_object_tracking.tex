Considering the objectives and conditions of the experiments for tracking an object, specifically that the object will move at a low speed and will not exhibit sudden changes in its trajectory or velocity, the author decided to employ the classical Kalman Filter to address the posed challenges. This method has demonstrated its speed and effectiveness in single-object tracking. Subsequently, the author will describe the principles and technical details of the Kalman Filter, drawing on the works "Visual object tracking -- classical and contemporary approaches" and "Object tracking: A survey."~\cite{VOTsurvay,VOTAhmad}

The Kalman filter(KF) is a statistical parametric recursive algorithm designed for discrete time systems, specifically linear dynamic systems. It uses a state space representation and operates in two steps: prediction and correction. The prediction step employs the state model to estimate the new state of the variables, while the correction step refines the estimates. The filter assumes the state to be distributed by a Gaussian. State space representation is shown in the following equations~\cite{VOTsurvay,VOTAhmad}: 
\begin{equation}
    \boldsymbol{X}_{n+1} = \boldsymbol{\Phi} \boldsymbol{X}_n + \boldsymbol{U}_n, 
\end{equation}

\begin{equation}
    \boldsymbol{Y}_n = \boldsymbol{M} \boldsymbol{X}_n + \boldsymbol{V}_n, 
\end{equation}

The state vector is represented by $\boldsymbol{X}_n$, with $\boldsymbol{\Phi}$ as the state transition matrix, $\boldsymbol{U}_n$ as the system noise vector, $\boldsymbol{V}_n$ as the observation noise vector, $\boldsymbol{Y}_n$ as the measurement vector and $\boldsymbol{M} $ as the observation matrix.~\cite{VOTAhmad}

The Kalman filter estimates the dynamic system states, accounting for noisy measurements (Gaussian noise) and model uncertainty. It operates in a prediction-correction cycle, using observed states to correct predicted states and update its gain matrix for improved future predictions, as outlined in equations (1.5)-(1.10).~\cite{VOTAhmad}

\begin{equation}
\boldsymbol{X}_{n|n}^* = \boldsymbol{X}_{n|n-1}^* + \boldsymbol{K}_n (\boldsymbol{Y}_n - \boldsymbol{M} \boldsymbol{X}_{n|n-1}^*)
\end{equation}

In this context, $\boldsymbol{X}_{n|n}^*$ denotes the posterior measurement, $\boldsymbol{X}_{n|n-1}^*$ signifies the prior measurement, and $\boldsymbol{K}_n$ represents the Kalman gain matrix, which is defined as~\cite{VOTAhmad}:

\begin{equation}
\boldsymbol{K}_n = \boldsymbol{S}_{n|n-1}^* \boldsymbol{M}^T [\boldsymbol{R}_n + \boldsymbol{M} \boldsymbol{S}_{n|n-1}^* \boldsymbol{M}^T]^{-1}
\end{equation}

In this case, $\boldsymbol{R}_n$ refers to the observation noise covariance calculated using equation (1.7), while $\boldsymbol{S}_{n|n-1}^*$ denotes the predictor error covariance defined by equation (1.8) where $E$ is the expected value.~\cite{VOTAhmad}

\begin{equation}
\boldsymbol{R}_n = \mathrm{COV}(\boldsymbol{V}_n) = E[\boldsymbol{V}_n \boldsymbol{V}_n^T]
\end{equation}

\begin{equation}
\boldsymbol{S}^*_n|_{n-1} = \mathrm{COV}(\boldsymbol{X}^*_n|_{n-1}) = \boldsymbol{\Phi} \boldsymbol{S}^*_{n-1|n-1} \boldsymbol{\Phi}^T + \boldsymbol{Q}_n
\end{equation}

\begin{equation}
\boldsymbol{S}^*_{n-1|n-1} = \mathrm{COV}(\boldsymbol{X}^*_{n-1|n-1}) = [\boldsymbol{I} - \boldsymbol{K}_{n-1} \boldsymbol{M}] \boldsymbol{S}^*_{n-1|n-2}
\end{equation}

In this context, $\boldsymbol{Q}_n$ represents the noise covariance matrix, which is calculated using equation (1.10)~\cite{VOTAhmad}.

\begin{equation}
\boldsymbol{Q}_n = \mathrm{COV}(\boldsymbol{U}_n) = E[\boldsymbol{U}_n \boldsymbol{U}_n^T]
\end{equation}

During the tracking process, the Kalman Filter operates in two modes:
\begin{enumerate}
    \item Normal tracking mode
    \item Occlusion mode~\cite{VOTAhmad}
\end{enumerate}

During normal tracking mode, KF predicts the next target coordinates in the image plane based on measurements to define the optimal search window. During occlusion mode, KF disregards the measured value and uses its predicted value for the next state prediction, effectively handling short-term occlusion.~\cite{VOTAhmad}

\begin{figure}[h]
    \caption{(a) Regular tracking, and (b) the challenging problem of occlusion, with the Kalman Filter effectively managing the situation~\cite{Springerfigures1} }
    \centering
    \includegraphics[width=0.8\textwidth]{text/images/kalmanTest.png}
    \label{fig:kalman_test}
\end{figure}

Figure ~\ref{fig:kalman_test}(a) demonstrates the normal tracking mode, while Figure ~\ref{fig:kalman_test}(b) displays the occlusion mode during tracking, showing that KF effectively addresses the occlusion issue.~\cite{VOTAhmad}

