import os
import shutil
import random

# Directories for images and labels
src_images_dir = 'voc/images/'
src_labels_dir = 'voc/labels/'

# Directories for train and test sets
train_images_dir = 'voc/train/images/'
train_labels_dir = 'voc/train/labels/'
test_images_dir = 'voc/test/images/'
test_labels_dir = 'voc/test/labels/'

# Create train and test directories if they don't exist
os.makedirs(train_images_dir, exist_ok=True)
os.makedirs(train_labels_dir, exist_ok=True)
os.makedirs(test_images_dir, exist_ok=True)
os.makedirs(test_labels_dir, exist_ok=True)

# Set the train-test split ratio
split_ratio = 0.7

# List all the image files
image_files = [f for f in os.listdir(src_images_dir) if f.endswith('.png')]

# Filter out images without corresponding labels
image_files = [f for f in image_files if os.path.isfile(src_labels_dir + os.path.splitext(f)[0] + '.xml')]

# Shuffle the image files
random.shuffle(image_files)

# Calculate the number of files for the train set
train_set_size = int(len(image_files) * split_ratio)

# Split the image files into train and test sets
train_image_files = image_files[:train_set_size]
test_image_files = image_files[train_set_size:]

# Copy the train images and labels to their respective directories
for image_file in train_image_files:
    shutil.copy(src_images_dir + image_file, train_images_dir + image_file)
    label_file = os.path.splitext(image_file)[0] + '.xml'
    shutil.copy(src_labels_dir + label_file, train_labels_dir + label_file)

# Copy the test images and labels to their respective directories
for image_file in test_image_files:
    shutil.copy(src_images_dir + image_file, test_images_dir + image_file)
    label_file = os.path.splitext(image_file)[0] + '.xml'
    shutil.copy(src_labels_dir + label_file, test_labels_dir + label_file)

print(f'Train and test sets have been created with a {split_ratio * 100:.0f}/{(1 - split_ratio) * 100:.0f} split.')
