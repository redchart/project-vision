import cv2
import sys

root_dir = "/Users/artemredchych/bakalar/project-vision"
sys.path.append(root_dir)

import libs.tracking.kalman as kalman
import libs.detection.final_detection as det


def distance_to_object(bbox_height_in_pixels, real_object_height=88, effective_focal_length=0.66, pixel_size=3.6):
    """
    Calculate the distance to the object from the camera.

    :param bbox_height_in_pixels: Height of the object in the image in pixels.
    :param real_object_height: Real height of the object in centimeters. Default is 88 cm.
    :param effective_focal_length: Effective focal length of the camera in millimeters. Default is 0.66 mm.
    :param pixel_size: Size of a pixel in micrometers. Default is 3.6 µm.

    :return: Distance to the object from the camera in centimeters.
    """
    # Convert pixel size to millimeters
    pixel_size_mm = pixel_size / 1000.0

    # Convert real_object_height to millimeters
    real_object_height_mm = real_object_height * 10

    # Calculate distance to object
    distance = (
        real_object_height_mm * effective_focal_length
    ) / (bbox_height_in_pixels * pixel_size_mm)

    # Convert distance to centimeters
    distance_cm = distance / 10

    return distance_cm

def init_kalman():
    return kalman.initialize_kalman_filter()

def print_distance(image, bbox, text_color=(0, 0, 0)):
    image_copy = image.copy()
    height, width, _ = image.shape
    ymin, xmin, ymax, xmax = bbox
    ymin = int(ymin * height)
    ymax = int(ymax * height)
    xmin = int(xmin * width)
    xmax = int(xmax * width)

    distance = distance_to_object(ymax - ymin)

    dist_label_x = int(xmin + 5)
    dist_label_y = int(ymin + ((ymax - ymin) / 2))
    dist_label = f'dist: {distance:.2f} cm'
    cv2.putText(
        image_copy,
        dist_label,
        (dist_label_x,
         dist_label_y),
        cv2.FONT_HERSHEY_SIMPLEX,
        0.5, (0, 153, 255), 1
    )

    return image_copy

def process_image(model, image, kf, is_first):
    # Run inference on the image
    output_dict = det.run_inference_for_single_image(model, image)
    # Draw bounding boxes and class names on the image
    detection = det.get_bounding_box(output_dict=output_dict, image=image, threshold=0.9)
    if detection is not None:
        bbox, class_name, score = detection
        result_image, distance = det.draw_detection_bbox(image, bbox, score, class_name)

        if is_first:
            initial_bbox = bbox
            # Set the initial state
            kf.statePost = kalman.bbox_to_state(initial_bbox)
        else:
            predicted_state = kf.predict()
            predicted_box = kalman.state_to_bbox(predicted_state)

            # Update the state with the new measurement
            kf.correct(kalman.bbox_to_state_oof(bbox))

            result_image = det.draw_predicted_bbox(result_image, predicted_box, (0, 0, 255))

        return result_image

    return None


