import numpy as np
import cv2
from six import BytesIO
import sys

root_dir = "/Users/artemredchych/bakalar/project-vision"  # Replace this with the actual path to the root directory
sys.path.append(root_dir)

from PIL import Image

import tensorflow as tf

def load_image_into_numpy_array(path):
    """Load an image from file into a numpy array.

    Puts image into numpy array to feed into tensorflow graph.
    Note that by convention we put it into a numpy array with shape
    (height, width, channels), where channels=3 for RGB.

    Args:
      path: a file path (this can be local or on colossus)

    Returns:
      uint8 numpy array with shape (img_height, img_width, 3)
    """
    img_data = tf.io.gfile.GFile(path, 'rb').read()
    image = Image.open(BytesIO(img_data))
    (im_width, im_height) = image.size
    return np.array(image.getdata()).reshape(
        (im_height, im_width, 1)).astype(np.uint8)


def run_inference_for_single_image(model, image):
    # The input needs to be a tensor, convert it using `tf.convert_to_tensor`.
    input_tensor = tf.convert_to_tensor(image)

    input_tensor = input_tensor[tf.newaxis,...]

    model_fn = model.signatures['serving_default']
    output_dict = model_fn(input_tensor)

    num_detections = int(output_dict.pop('num_detections'))
    output_dict = {key:value[0, :num_detections].numpy()
                   for key,value in output_dict.items()}
    output_dict['num_detections'] = num_detections

    output_dict['detection_classes'] = output_dict['detection_classes'].astype(np.int64)

    return output_dict

def draw_predicted_bbox(image, predicted_bbox, color=(0, 255, 0), thickness=2):
    """
    Draw a rectangle on the given image based on the predicted bounding box.

    Args:
        image (numpy.array): The input image.
        predicted_bbox (tuple): The predicted bounding box in the format (x, y, w, h).
        color (tuple, optional): The color of the rectangle (B, G, R). Default is green (0, 255, 0).
        thickness (int, optional): The thickness of the rectangle border. Default is 2.

    Returns:
        numpy.array: The image with the rectangle drawn.
    """
    ymin, xmin, ymax, xmax = predicted_bbox


    cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color, 2)

    return image

def distance_to_object(image_height_in_pixels, real_object_height=88, effective_focal_length=0.66, pixel_size=3.6):
    """
    Calculate the distance to the object from the camera.

    :param image_height_in_pixels: Height of the object in the image in pixels.
    :param real_object_height: Real height of the object in centimeters. Default is 88 cm.
    :param effective_focal_length: Effective focal length of the camera in millimeters. Default is 0.66 mm.
    :param pixel_size: Size of a pixel in micrometers. Default is 3.6 µm.

    :return: Distance to the object from the camera in centimeters.
    """
    # Convert pixel size to millimeters
    pixel_size_mm = pixel_size / 1000.0

    # Convert real_object_height to millimeters
    real_object_height_mm = real_object_height * 10

    # Calculate distance to object
    distance = (real_object_height_mm * effective_focal_length) / (image_height_in_pixels * pixel_size_mm)

    # Convert distance to centimeters
    distance_cm = distance / 10

    return distance_cm

def draw_detection_bbox(image, box, score, class_name, box_color=(0, 255, 0), text_color=(0, 0, 0)):
    image_copy = image.copy()
    height, width, _ = image.shape
    ymin, xmin, ymax, xmax = box

    print(f"ymin: {ymin}, ymax: {ymax}, xmin: {xmin}, xmax: {xmax}")

    distance = distance_to_object(ymax - ymin)

    cv2.rectangle(image_copy, (xmin, ymin), (xmax, ymax), box_color, 2)
    label = f'{class_name}: {score:.2f}'
    label_size, base_line = cv2.getTextSize(label, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1)
    label_ymin = max(ymin, label_size[1] + 10)
    cv2.rectangle(image_copy, (xmin, label_ymin - label_size[1] - 10),
                  (xmin + label_size[0], label_ymin + base_line - 10), box_color, cv2.FILLED)
    cv2.putText(image_copy, label, (xmin, label_ymin - 7), cv2.FONT_HERSHEY_SIMPLEX, 0.5, text_color, 1)

    dist_label_x = int(xmin + 5)
    dist_label_y = int(ymin + ((ymax - ymin) / 2))
    dist_label = f'dist: {distance:.2f} cm'
    cv2.putText(image_copy, dist_label, (dist_label_x, dist_label_y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0,153,255), 1)

    return image_copy, distance


def get_bounding_box(output_dict, image,  threshold=0.9):
    category_index = {1: {'id': 1, 'name': 'teddy'}}
    num_detections = output_dict['num_detections']

    for i in range(num_detections):
        class_id = output_dict['detection_classes'][i]
        score = output_dict['detection_scores'][i]

        if score > threshold:
            box = output_dict['detection_boxes'][i]
            height, width, _ = image.shape
            ymin, xmin, ymax, xmax = box
            ymin = int(ymin * height)
            ymax = int(ymax * height)
            xmin = int(xmin * width)
            xmax = int(xmax * width)
            class_name = category_index[class_id]['name']

            return [[ymin, xmin, ymax, xmax], class_name, score]

    return None


def load_model(model_directory):
    # model_directory = './inference_graph'
    model = tf.saved_model.load(f'{model_directory}/saved_model')
    return model

def calculate_iou(box1, box2):
    x1, y1, w1, h1 = box1
    x2, y2, w2, h2 = box2

    # Calculate the (x, y) coordinates of the intersection rectangle
    xi1 = max(x1, x2)
    yi1 = max(y1, y2)
    xi2 = min(x1 + w1, x2 + w2)
    yi2 = min(y1 + h1, y2 + h2)

    # Calculate the area of intersection rectangle
    inter_area = max(xi2 - xi1, 0) * max(yi2 - yi1, 0)

    # Calculate the area of each bounding box
    box1_area = w1 * h1
    box2_area = w2 * h2

    # Calculate the union of both bounding boxes
    union_area = box1_area + box2_area - inter_area

    # Calculate the Intersection over Union (IoU) value
    iou = inter_area / union_area if union_area != 0 else 0

    return iou

