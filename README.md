# project-vision



## Getting started

In this repository one can find implementation part of thesis "VISUAL OBJECT
DETECTION AND
TRACKING BY THE
CRAZYFLIE
QUADCOPTER"

Repository has the following structure:
- README.md
- text
    - thesis.pdf (text of this thesis in PDF format)
    - thesis (source of this thesis in LaTeX)
- data
    - lab_test_raw (raw data from the tests)
    - lab_test_result (detection and tracking results)
    - training (training dataset with images and labels)
- inference_graph (stored model for object detection)
- crazyflie-lib-python (python library for crazyflie)
- libs
    - detection (scripts used in object detection)
    - pos (scripts used in drone flying)
    - tracking (scripts used in object tracking)
    - models (TensorFlow libraries)
- utility (useful scripts)
- flying_streamer.py (real-time object detection and tracking streamer)
- requirements.txt (Python requirements)
- result.mp4 (detection and tracking result in MP4 format)
- result.gif (detection and tracking result in GIF format)
- lab_test_video.MOV (video of the flying drone from the laboratory)


Result of object detection, tracking and distance estimation is shown on
![Alt text for the GIF](result.gif)
